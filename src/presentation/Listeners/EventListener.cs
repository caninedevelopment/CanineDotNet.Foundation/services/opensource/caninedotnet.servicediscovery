﻿using CreateOrUpdate = Canine.Net.ServiceDiscovery.Application.Resources.Services.Commands.CreateOrUpdate;
using Canine.Net.Infrastructure.RabbitMQ;
using Canine.Net.Infrastructure.RabbitMQ.Common.Event;
using Canine.Net.Infrastructure.RabbitMQ.Message;
using Canine.Net.ServiceDiscovery.Domain.Repositories;

namespace Canine.Net.ServiceDiscoveryService.Listeners
{
    public class EventListener : IEventListener
    {
        private readonly ServiceRepository serviceRepository;

        public EventListener(ServiceRepository serviceRepository)
        {
            this.serviceRepository = serviceRepository;
        }
        public Constants.EventHandler Handler => Handle;
        private void Handle(string[] topic, ReturnMessage dto)
        {
            if(topic.Length == 2 && topic[0] == "servicediscovery" && topic[1] == "register") {
                var command = new CreateOrUpdate.Command(serviceRepository);
                var request = new CreateOrUpdate.Request { Message = dto };
                request.Validate();
                command.Execute(request);
            }
        }
    }
}
