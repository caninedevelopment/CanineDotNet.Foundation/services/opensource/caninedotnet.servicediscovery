﻿using System.Linq;
using Canine.Net.Infrastructure.Console;
using Canine.Net.Infrastructure.RabbitMQ.Common.Event;
using Microsoft.Extensions.DependencyInjection;
using static Canine.Net.ServiceDiscovery.Infrastructure.Persistence.PostgreSQL.DependencyInjection;
using static Canine.Net.ServiceDiscovery.Implementation.DependencyInjection;
using static Canine.Net.Infrastructure.Console.Program;
using Canine.Net.Infrastructure.Config;

namespace Canine.Net.ServiceDiscoveryService
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var pwd = System.Environment.GetEnvironmentVariable("sqlserver_password");
            var usr = System.Environment.GetEnvironmentVariable("sqlserver_user");
            var databasename = System.Environment.GetEnvironmentVariable("sqlserver_databasename");
            var port = System.Environment.GetEnvironmentVariable("sqlserver_port");
            var servername = System.Environment.GetEnvironmentVariable("sqlserver_name");

            var rabbitMQUsername = System.Environment.GetEnvironmentVariable("rabbitmq_username");
            var rabbitMQPassword = System.Environment.GetEnvironmentVariable("rabbitmq_password");
            var rabbitMQHost = System.Environment.GetEnvironmentVariable("rabbitmq_host");
            var rabbitMQPort = System.Environment.GetEnvironmentVariable("rabbitmq_port");

            RabbitMQSettings rabbitSet = new RabbitMQSettings(rabbitMQUsername, rabbitMQPassword, rabbitMQHost, rabbitMQPort != null ? int.Parse(rabbitMQPort) : 5672);

            ServiceProvider serviceProvider = new ServiceCollection().AddImplementation().AddPostgreSQLPersistenceProvider(servername, usr, pwd, int.Parse(port), databasename).AddPresentation().BuildServiceProvider();

            StartRabbitMq("ServiceDiscovery", new ProgramVersion(1), serviceProvider.GetServices<IResource>().ToList(), serviceProvider.GetServices<IEventListener>().ToList(), rabbitSet);
        }
    }
}
