﻿namespace Canine.Net.ServiceDiscoveryService
{
    using Canine.Net.Infrastructure.Console;
    using Canine.Net.Infrastructure.RabbitMQ.Common.Event;
    using Canine.Net.ServiceDiscoveryService.Listeners;
    using Canine.Net.ServiceDiscoveryService.Resources;
    using Microsoft.Extensions.DependencyInjection;
    public static class DependencyInjection
    {
        public static IServiceCollection AddPresentation(this IServiceCollection services)
        {
            services.AddTransient<IResource, ServicesResource>();
            services.AddTransient<IEventListener, EventListener>();
            return services;
        }
    }
}
