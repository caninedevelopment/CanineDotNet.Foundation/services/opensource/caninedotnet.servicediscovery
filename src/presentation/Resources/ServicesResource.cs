﻿using Canine.Net.ServiceDiscovery.Application.Common.Interfaces;
using Canine.Net.Infrastructure.Console;
using Canine.Net.ServiceDiscovery.Domain.Repositories;
using Command = Canine.Net.ServiceDiscovery.Application.Resources.Services.Commands;
namespace Canine.Net.ServiceDiscoveryService.Resources
{
    internal class ServicesResource : BaseResource
    {
        public ServicesResource(ServiceRepository serviceRepository, ICanineService canineService) : base("Services")
        {
            this.AddCommand(new Command.GetAll.Command(serviceRepository), OperationType.Get, "GetAll", "Get documentation of the known services on the network");
            this.AddCommand(new Command.Scan.Command(serviceRepository, canineService), OperationType.Execute, "Scan", "Scan the network for services");
        }
    }
}
